#!/bin/bash

# Define the levels of nagios warnings.
OKLEVEL=0
WARNINGLEVEL=1
CRITICALLEVEL=5
PUPPETMON_FQDN="https://puppetmon.cgca.uwm.edu/nagios"


# Gets the nagios page output and puts it in a variable
# EXAMPLE OUTPUT:
# #NAGIOS
# 
# versions-ligo.cgca.uwm.edu
# puppetmon.cgca.uwm.edu
# vpn.cgca.uwm.edu
# downes-stretch.cgca.uwm.edu
OUTPUT=$(curl -s $PUPPETMON_FQDN)

# Check if output contains "#NAGIOS" (should be first line). 
# If not, puppetmon is down or acting up so return UNKNOWN
if [[ $OUTPUT =~ "#NAGIOS" ]]; then

	# Counts up the FQDNs returned by the check
	# Skips the first line (which should be "#NAGIOS") and any empty lines
	WC=$(echo "$OUTPUT" | tail -n +2 | wc -w)

	if [[ $WC > $WARNINGLEVEL ]]; then
		echo "WARNING: $WC nodes disconnected from puppet.|lostnodes=$WC"
		echo "See https://puppetmon.cgca.uwm.edu/nagiosreport for more information."
		echo $(echo "$OUTPUT" | tail -n +2)
		exit 1
	elif [[ $WC > $CRITICALLEVEL ]]; then
		echo "CRITICAL: $WC nodes disconnected from puppet!|lostnodes=$WC"
		echo "See https://puppetmon.cgca.uwm.edu/nagiosreport for more information."
		echo $(echo "$OUTPUT" | tail -n +2)
		exit 2
	else # $WC <= $OKLEVEL
		echo "OK: $WC nodes not connected to puppet.|lostnodes=$WC"
		echo "See https://puppetmon.cgca.uwm.edu/nagiosreport for more information."
		exit 0
	fi

else # The check is down, return UNKNOWN
	echo "UNKNOWN: Invalid puppetmon nagios endpoint or it's not responding."
	echo "See https://puppetmon.cgca.uwm.edu/nagiosreport for more information."
	exit 3
fi

