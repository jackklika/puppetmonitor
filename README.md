# Puppetmonitor 
The aim of this project is to provide a simple webpage that takes puppet json reports, turns them into go objects, and displays their status.

This is a golang project which is compiled into a binary which can be run on a machien to connect to a puppetdb.

Although created specifically for UWM's CGCA, this should work in any setup that matches the assumptions at the bottom. Even if it doesn't, you may only need to edit a bit of code.

## Configuration
Edit the ``conf/app.conf`` file for configuration such as HTTP port, puppetdb path, etc. 

## Assumptions
As this was written for a specific system, there are some assumptions within this repo and the code.
- Your puppetdb instance is reachable by the server this will run on, and your puppetdb supports v4 queries
- The server this runs on is a puppet-controlled machine, and has SSL certs at /etc/puppetlabs/puppet/ssl that can query puppetdb and are named after their FQDN, eg "klikadev.cgca.uwm.edu.pem". If you're having problems with authentication, please check out nodes/tls-client where the paths to the certificates and keys are declared.
- If using the binary within this git repo (included for your convenience!), your machine needs to be x86-64. Otherwise you'll have to configure a go environment and compile for whatever target you need.
- The revdns module will not be available due to being specifically for our environment.
