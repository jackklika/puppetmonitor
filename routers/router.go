package routers

import (
	"git.ligo.org/john.klika/puppetmonitor/controllers"
	"github.com/astaxie/beego"
)

// This simply handles how http requests are handled.
func init() {
	beego.Router("/", &controllers.MainController{})
	beego.Router("/revdns", &controllers.MainController{}, "get:RevDNS")
	beego.Router("/nodes", &controllers.MainController{}, "get:IndNode")
	beego.Router("/nodes/:certname", &controllers.MainController{}, "get:IndNode")
	beego.Router("/facts", &controllers.FactsController{})
	beego.Router("/facts/:fact", &controllers.FactsController{})
	beego.Router("/about", &controllers.MainController{}, "get:About")
	beego.Router("/nagios", &controllers.MainController{}, "get:Nagios")
	beego.Router("/nagiosreport", &controllers.MainController{}, "get:NagiosReport")
}
