package main

import (
	"git.ligo.org/john.klika/puppetmonitor/controllers"
	"git.ligo.org/john.klika/puppetmonitor/nodes"
	"git.ligo.org/john.klika/puppetmonitor/revdns"
	_ "git.ligo.org/john.klika/puppetmonitor/routers"
	"github.com/astaxie/beego"
)

// Main activity loop
func main() {

	// Start the activity loop
	go nodes.Activityloop()

	// Run the Reverse DNS activity loop
	go revdns.RevDNSActivity()

	// Handle Errors
	beego.ErrorController(&controllers.ErrorController{})

	beego.AddFuncMap("HumanTime", nodes.HumanTime)
	beego.AddFuncMap("ColorTime", nodes.ColorTime)

	// START BEEGO
	beego.Run()

}
