package controllers

import (
	"git.ligo.org/john.klika/puppetmonitor/nodes"
	"git.ligo.org/john.klika/puppetmonitor/revdns"
	"github.com/astaxie/beego"
	"time"
)

func (c *MainController) Nagios() {
	c.TplName = "nagios.tpl"

	c.Data["PuppetizedList"] = RunNagios()
}

func (c *MainController) NagiosReport() {
	c.TplName = "nagiosreport.tpl"

	c.Data["PuppetizedList"] = RunNagios()
	c.Data["nagios_maxtime"] = beego.AppConfig.String("nagios_maxtime")
}

// Runs the nagios check, returns a slice of DNSResult objects
// matching a specific criteria.
// This probably should go in the revdns package at some point.
func RunNagios() []nodes.Node {

	var puppetized []nodes.Node

	// Let's check these nodes against the persistnodes.
	// Only keep nodes that:
	// Are puppetized AND
	// (Have a `LatestReportStatus` of failed OR
	//  Have been inactive for more than 1h15m)
	for _, n := range revdns.DNSList {

		tmp, err := nodes.Search(n.FQDN)
		if err != nil {
			// Node is in persistnodes but not DNS nodes. Skip!
			continue
		}

		timelimit, err := time.ParseDuration(beego.AppConfig.String("nagios_maxtime"))
		if err != nil {
			beego.Critical(`Configuration nagios_maxtime is not a valid time. 
				Please format it according to time.ParseDuration`)
		}

		if n.IsPuppetized == "Yes" && (tmp.LatestReportStatus == "failed" ||
			// Is the timestamp before the current time minus the configured timelimit?
			(tmp.CatalogTimestamp.Before(time.Now().Add(-timelimit)))) {

			// Add the node to the return list
			puppetized = append(puppetized, tmp)
		}
	}

	return puppetized
}
