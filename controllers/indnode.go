package controllers

import (
	"git.ligo.org/john.klika/puppetmonitor/nodes"
)

// Handles the individual node view
func (c *MainController) IndNode() {

	// If URL was /nodes/k.cgca.uwm.edu,
	// certname == k.cgca.uwm.edu
	certname := c.Ctx.Input.Param(":certname")

	if certname == "" {
		c.TplName = "404.tpl"
		return
	}

	// Outnode is of type "IndNodeMap"
	outnodemap, err := nodes.GetIndNodeMap(certname)
	if err != nil {
		c.TplName = "404.tpl"
		return
	}

	c.TplName = "indnode.tpl"
	c.Data["Certname"] = certname
	c.Data["IndNodeMap"] = outnodemap

}
