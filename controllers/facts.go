package controllers

import (
	"fmt"
	"git.ligo.org/john.klika/puppetmonitor/nodes"
	"github.com/astaxie/beego"
)

type FactsController struct {
	beego.Controller
}

// @router /facts/:fact
func (c *FactsController) Get() {

	fact := c.Ctx.Input.Param(":fact")
	// If URL was /facts/uptime,
	// fact == uptime
	beego.Trace(fmt.Sprintf("Fact [%s]", fact))

	if fact == "" {
		c.TplName = "factshome.tpl"
		c.Data["FactList"] = nodes.GetFactList()
		return
	}

	// Outnode is of type "IndNodeMap"
	factstruct, err := nodes.GetFacts(fact)
	if err != nil {
		c.TplName = "404.tpl"
		return
	}

	c.TplName = "facts.tpl"
	c.Data["FactStruct"] = factstruct
	c.Data["Fact"] = fact

}

func (c *FactsController) Post() {
	c.Redirect(fmt.Sprintf("/facts/%s", c.GetString("fact")), 302)
}
