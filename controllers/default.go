package controllers

import (
	"git.ligo.org/john.klika/puppetmonitor/nodes"
	"git.ligo.org/john.klika/puppetmonitor/revdns"
	"github.com/astaxie/beego"
	"time"
)

type ErrorController struct {
	beego.Controller
}
type MainController struct {
	beego.Controller
}

// Root page, aka puppet monitor
func (c *MainController) Get() {
	c.TplName = "index.tpl"
	c.Data["Persistnodes"] = nodes.Persistnodes
	c.Data["Now"] = time.Now()
	c.Data["IsError"] = nodes.IsError
}

// Reverse DNS page
func (c *MainController) RevDNS() {
	c.TplName = "revdns.tpl"
	c.Data["RevNodes"] = revdns.DNSList
}

// About page
func (c *MainController) About() {
	c.TplName = "about.tpl"
	c.Data["Version"] = beego.AppConfig.String("version")
}

// 404 Error handling
func (c *ErrorController) Error404() {
	c.TplName = "404.tpl"
}
