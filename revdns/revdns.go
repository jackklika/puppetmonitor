package revdns

import (
	"fmt"
	"git.ligo.org/john.klika/puppetmonitor/nodes"
	"net"
	"time"
)

var DNSList []DNSResult

type DNSResult struct {
	FQDN         string
	IP           string
	IsPuppetized string
}

// The daemon that runs the reverse DNS searches
func RevDNSActivity() {

	for { //forever
		time.Sleep(5 * time.Second)
		generaterevnodes()
		time.Sleep(5 * time.Second)
	}
}

// Generates the reverse dns nodes
func generaterevnodes() {
	var tmpnames []DNSResult
	for i := 1; i < 255; i++ {
		ip := fmt.Sprintf("129.89.61.%d", i)
		result, err := net.LookupAddr(ip)
		if err != nil {
			// Probably not found. This is expected sometimes.
		} else {
			tmpnames = append(tmpnames, createDNSResult(result[0], ip))
		}
	}
	DNSList = tmpnames
}

// Creates a "DNSResult" struct. Checks puppetization by matching against
// persistant nodes found in the nodes package.
func createDNSResult(fqdn string, ip string) DNSResult {
	isPuppetized := "No"
	for _, n := range nodes.Persistnodes {
		if (n.Name + ".") == fqdn {
			isPuppetized = "Yes"
			break
		}
	}
	return DNSResult{
		FQDN:         fqdn[:len(fqdn)-1],
		IP:           ip,
		IsPuppetized: isPuppetized,
	}
}
