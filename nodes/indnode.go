// Handles Individual Nodes. This will pull some facts into a "IndNode" object
// and takes a certname as an argument.
package nodes

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/astaxie/beego"
)

type IndNode []struct {
	Certname string      `json:"certname"`
	Name     string      `json:"name"`
	Value    interface{} `json:"value"`
}

func GetIndNodeMap(certname string) (map[string]interface{}, error) {

	// Creates a query which queries a specific node's facts.
	query := beego.AppConfig.String("puppetdb") + fmt.Sprintf("/pdb/query/v4/nodes/%s/facts", certname)
	queryout, err := letstls(query)
	if len(queryout) < 50 || err != nil {
		return nil, errors.New("Could not find node!")
	}

	var indnode IndNode
	json.Unmarshal([]byte(queryout), &indnode)

	indnodemap := make(map[string]interface{})

	for i := range indnode {
		indnodemap[indnode[i].Name] = indnode[i].Value
	}
	return indnodemap, nil

}
