// Handles Individual Nodes. This will pull some facts into a "IndNode" object
// and takes a certname as an argument.
package nodes

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/astaxie/beego"
	"github.com/patrickmn/sortutil"
)

/* This is included in nodes/indnode.go
type IndNode []struct {
	Certname	string		`json:"certname"`
	Name		string		`json:"name"`
	Value		interface{}	`json:"value"`
}
*/

// Queries facts, returns an indnode struct with the node's facts
func GetFacts(factname string) (IndNode, error) {

	// Creates a query which queries a specific node's facts.
	query := beego.AppConfig.String("puppetdb") + fmt.Sprintf("/pdb/query/v4/facts/%s", factname)

	queryout, err := letstls(query)

	beego.Trace("Finished Facts Query ", query)
	if len(queryout) < 50 || err != nil {
		return nil, errors.New("Could not find node!")
	}
	var factstruct IndNode
	json.Unmarshal([]byte(queryout), &factstruct)

	sortutil.AscByField(factstruct, "Certname")
	return factstruct, nil

}

// Returns an array of every fact according to the "fact-names" query.
func GetFactList() []string {
	query := beego.AppConfig.String("puppetdb") + "/pdb/query/v4/fact-names"
	queryout, err := letstls(query)
	if len(queryout) < 30 || err != nil {
		return nil
	}

	var factlist []string
	json.Unmarshal([]byte(queryout), &factlist)

	return factlist

}
