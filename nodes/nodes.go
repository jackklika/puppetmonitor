// This will be the node object.

package nodes

import (
	"fmt"
	"math"
	"strings"
	"time"
)

// This is what we store nodes as in memory.
type Node struct {

	// Certname
	Name string

	// changed? unchanged? failed?
	LatestReportStatus string

	// Catalog Environement -- the node's environment
	CatalogEnvironment string

	// Catalog Timestamp
	CatalogTimestamp time.Time

	// Cached catalog status of the last puppet run for the node.
	// Possible values: explicitly_requested, on_failure, not_used, or null
	CachedCatalogStatus string

	// Is it being reported, or is it being pulled from persistance?
	IsRecorded string
}

// Converts the messy json-parsed NodeArr struct from nodes/nodesd.go
// into a nice Node object we will work with.
func nodeArrToNode(na NodeArr, name string) Node {

	n := na[0] //nasty hack, just need to get na[0]'s type.
	for _, a := range na {
		if a.Certname == name {
			n = a
			break
		}
	}

	return Node{
		Name:                n.Certname,
		LatestReportStatus:  n.LatestReportStatus,
		CatalogEnvironment:  n.CatalogEnvironment,
		CatalogTimestamp:    n.CatalogTimestamp,
		CachedCatalogStatus: n.CachedCatalogStatus,
		IsRecorded:          "false",
	}
}

// Takes a time, turns it into a number for use in color conditional formatting
func ColorTime(t time.Time) string {
	delta := time.Since(t)
	diff := 300 - delta.Nanoseconds()/time.Minute.Nanoseconds()
	return fmt.Sprintf("%v", diff)
}

// Returns the time since 't' in a pretty hh:ss.
func HumanTime(t time.Time) string {
	if t.UnixNano() == -6795364578871345152 {
		return ("null")
	}

	delta := time.Since(t)
	var strreturn string

	sec := delta.Nanoseconds() / time.Second.Nanoseconds()
	min := delta.Nanoseconds() / time.Minute.Nanoseconds()
	hour := delta.Nanoseconds() / time.Hour.Nanoseconds()
	day := math.Floor(float64(hour) / 24)

	numreturn := sec // hack to get sec's type easily
	if sec < 60 {
		numreturn = sec
		strreturn = "seconds"
	} else if min < 60 {
		numreturn = min
		strreturn = "minutes"
	} else if hour < 24 {
		numreturn = hour
		strreturn = "hours"
	} else {
		numreturn = int64(day)
		strreturn = "days"
	}

	// Handle plurals
	if numreturn == 1 {
		strreturn = strings.TrimSuffix(strreturn, "s")
	}

	return fmt.Sprintf("%v %s", numreturn, strreturn)

}
