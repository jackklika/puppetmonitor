package nodes

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/astaxie/beego"
	"github.com/patrickmn/sortutil"
	"io"
	"io/ioutil"
	"os"
	"strings"
	"time"
)

var (
	Stagednodes  []Node // Nodes read from the query
	Persistnodes []Node // Nodes from persistant storage
	IsError      string
)

// This will be what checks for updates and such on the main page
func Activityloop() {

	beego.Trace("Activity loop started")

	// This reads the nodes from the query and persistant storage
	update()

	for {
		time.Sleep(10 * time.Second)
		err := update()
		if err != nil {
			IsError = fmt.Sprintf("ERROR FOUND: %v", err)
		} else {
			IsError = ""
		}
	}

}

// Will update the persistant nodes. This is the core logic!
func update() error {

	// opening persistance file, storing its contents in 'persistfilestr'
	persistfile, _ := os.OpenFile(beego.AppConfig.String("persistfile"), os.O_RDWR, 0666)
	defer persistfile.Close()
	bytes, _ := ioutil.ReadFile(beego.AppConfig.String("persistfile"))
	persistfilestr := string(bytes)

	// If there's not much in the file, it probably means nothing was written to it yet.
	// In that case, we should populate it with the query's nodes.
	if len(persistfilestr) < 15 {
		tmp, err := queryAndConvert()

		if err != nil {
			return err
		}

		outstr, _ := json.Marshal(tmp)
		io.WriteString(persistfile, string(outstr))
		fmt.Println("Wrote to persistance file")
	}

	// Staged nodes will equal the queried
	Stagednodes, err := queryAndConvert()

	if err != nil {
		return err
	}

	nodesread, _ := ioutil.ReadFile(beego.AppConfig.String("persistfile"))
	json.Unmarshal(nodesread, &Persistnodes)

	// At this point, Stagednodes is the queried nodes,
	// and Persistnodes is the persisted nodes

	var nodelist []Node

	// Iterate through staged nodes, adding each one to the nodelist.
	for _, sn := range Stagednodes {
		sn.IsRecorded = "true" // If it's in the returned query, it's recorded.
		nodelist = append(nodelist, sn)
	}

	// Iterate through Persistnodes and check if there's any in Persistnodes but not Stagednodes
	// If Stagednodes does not contain a node from Persistnodes, add it to the nodelist
	var isfound bool
	for _, pn := range Persistnodes {
		isfound = false
		for _, sn := range Stagednodes {
			if pn.Name == sn.Name {
				isfound = true
			}
		}
		if isfound == false {
			pn.IsRecorded = "false"
			nodelist = append(nodelist, pn)
		}
	}

	// persistnode back to textfile
	// This could go into a "file management" package?
	Persistnodes = nodelist
	txtout, _ := json.Marshal(Persistnodes)
	ioutil.WriteFile(beego.AppConfig.String("persistfile"), txtout, 0666)

	// This might need to go into the controller
	sortutil.AscByField(Persistnodes, "Name")
	sortutil.DescByField(Persistnodes, "IsRecorded")

	return nil // No errors
}

// Reads the query, convers them to nodes array.
func queryAndConvert() ([]Node, error) {

	var toreturn []Node
	var toconvert NodeArr

	query := beego.AppConfig.String("puppetdb") + "/pdb/query/v4/nodes" // constructs the puppetdb query

	queryout, err := letstls(query)
	if err != nil {
		return nil, err
	}

	json.Unmarshal([]byte(queryout), &toconvert) // Throws the node array into 'toconvert'

	// Converts the NodeArr into []Node
	for _, n := range toconvert {
		tmp := nodeArrToNode(toconvert, n.Certname)
		toreturn = append(toreturn, tmp)
	}

	return toreturn, nil

}

// Returns a node matching a fqdn from persistnodes
// This eventually needs to go into nodes.go as it is a nodes function.
func Search(fqdn string) (node Node, err error) {
	for _, n := range Persistnodes {
		// removes prefixing or trailing periods, common with dns results
		if strings.Trim(n.Name, ".") == strings.Trim(fqdn, ".") {
			return n, nil
		}
	}
	return Node{}, errors.New("Not found")
}

// This is the raw format when the json is received.
// Another, simpler node struct is created from this in node.go
// We need this struct.
type NodeArr []struct {
	Deactivated                  interface{} `json:"deactivated"`
	LatestReportHash             string      `json:"latest_report_hash"`
	FactsEnvironment             string      `json:"facts_environment"`
	CachedCatalogStatus          string      `json:"cached_catalog_status"`
	ReportEnvironment            string      `json:"report_environment"`
	LatestReportCorrectiveChange interface{} `json:"latest_report_corrective_change"`
	CatalogEnvironment           string      `json:"catalog_environment"`
	FactsTimestamp               time.Time   `json:"facts_timestamp"`
	LatestReportNoop             bool        `json:"latest_report_noop"`
	Expired                      interface{} `json:"expired"`
	LatestReportNoopPending      bool        `json:"latest_report_noop_pending"`
	ReportTimestamp              time.Time   `json:"report_timestamp"`
	Certname                     string      `json:"certname"`
	CatalogTimestamp             time.Time   `json:"catalog_timestamp"`
	LatestReportStatus           string      `json:"latest_report_status"`
}
