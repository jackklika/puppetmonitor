package nodes

import (
	"crypto/tls"
	"crypto/x509"
	"flag"
	"io/ioutil"
	"log"
	"net/http"
	"os/exec"
	"strings"
	"time"
)

var fqdn []byte

// Assuming we are using the typical puppet certificate naming convention
// Change these paths if you are having issues with auth.
var (
	certFile = flag.String("cert", "/etc/puppetlabs/puppet/ssl/certs/"+getfqdn()+".pem", "a pem eoncoded certificate file.")
	keyFile  = flag.String("key", "/etc/puppetlabs/puppet/ssl/private_keys/"+getfqdn()+".pem", "a pem encoded private key file.")
	caFile   = flag.String("ca", "/etc/puppetlabs/puppet/ssl/certs/ca.pem", "a pem eoncoded ca's certificate file.")
)

// Runs `hostname --fqdn` on this server. Used above to get the certificate
func getfqdn() string {
	fqdn, _ := exec.Command("hostname", "--fqdn").Output()
	return strings.TrimSpace(string(fqdn[:]))
}

// Makes a proper tls request
func letstls(stringin string) (string, error) {

	flag.Parse()

	// Load client cert
	cert, err := tls.LoadX509KeyPair(*certFile, *keyFile)
	if err != nil {
		// This is not proper error handling and is not "idiomatic go".
		// But it should work fine for the final iteration of this application.
		log.Println(err)
		return "CLIENT CERT ERROR", err
	}

	// Load CA cert
	caCert, err := ioutil.ReadFile(*caFile)
	if err != nil {
		log.Println(err)
		return "CA CERT ERROR", err
	}
	caCertPool := x509.NewCertPool()
	caCertPool.AppendCertsFromPEM(caCert)

	// Setup HTTPS client
	tlsConfig := &tls.Config{
		Certificates: []tls.Certificate{cert},
		RootCAs:      caCertPool,
	}
	tlsConfig.BuildNameToCertificate()
	transport := &http.Transport{TLSClientConfig: tlsConfig}
	client := &http.Client{
		Transport: transport,
		Timeout: time.Duration(5 * time.Second),
	}

	// Do GET something
	resp, err := client.Get(stringin)
	if err != nil {
		log.Println(err)
		return "GET ERROR", err
	}
	defer resp.Body.Close()

	// Dump response
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Println(err)
		return "RESPONSE DUMP ERROR", err
	}
	return string(data), nil
}
