<!DOCTYPE html>
<html>
<head>
	<title>Puppet Monitor</title>
	{{ assets_css "/static/css/revdnsview.css" }}
	{{ assets_css "/static/css/style.css" }}
</head>
<body>
	<div class="header">
		<a href=/>Home</a> |
		<a href=/revdns>61VLAN Reverse DNS</a> |
		<a href=/facts>Facts</a> |
		<a href=/about>About</a>
	</div>
	<div class="desc">
		<h1>RevDNS</h1>
		<p>Here we're doing a reverse DNS search on the 129.89.61 VLAN and showing which ones report to the puppetmaster, equivalent to iterating through the results of <code>for i in `seq 2 255`; dig +short -x 129.89.61.$i</code></p>
		<p>Due to timing, the results of this page are inaccurate for the first 60 seconds this web application runs.</p>
	</div>

	<table class="main">
		<tr>
			<th>IP</th>
			<th>FQDN</th>
			<th>Puppetized?</th>
		</tr>
		{{ range .RevNodes }}
		<tr>
			<tr class="{{ .IsPuppetized }}">
				<td>{{ .IP }}</td>
				<td><a href="/nodes/{{.FQDN}}">{{.FQDN}}</a></td>
				<td>{{ .IsPuppetized }}</td>	
		</tr>
		{{ end }}
	</table>
	<hr>

</body>
</html>
