<!DOCTYPE html>
<html>
<head>
	<title>Puppet Monitor</title>
</head>
<body>
	<div class="header">
		<a href=/>Home</a> |
		<a href=/revdns>61VLAN Reverse DNS</a> |
		<a href=/facts>Facts</a> |
		<a href=/about>About</a>
	</div>
	<hr>
	<div class="desc">
		<h1>{{.Certname}}</h1>
	</div>
	<h2>OS Info</h2>
		<li>OS: {{ map_get .IndNodeMap "os" "name" }}</li>
		<li>Release: {{ map_get .IndNodeMap "os" "distro" "release" "full" }}</li>
		<li>Kernel Version: {{ map_get .IndNodeMap "kernelversion" }}</li>
	<h2>Server Info</h2>
		<li>Memory Used / Total: {{ map_get .IndNodeMap "memory" "system" "used" }}/{{ map_get .IndNodeMap "memory" "system" "total" }}</li>
		<li>Uptime: {{ map_get .IndNodeMap "system_uptime" "uptime" }}</li>
		<li>Load Averages (1m/5m/15m): {{ map_get .IndNodeMap "load_averages" "1m"}}/{{ map_get .IndNodeMap "load_averages" "5m"}}/{{ map_get .IndNodeMap "load_averages" "15m"}}</li>
	<h2>Puppet Info</h2>
		<li>Client: {{ map_get .IndNodeMap "clientversion" }}</li>
	<h2>Network Info</h2>
		<li>eth0 IP: {{ map_get .IndNodeMap "networking" "interfaces" "eth0" "ip" }}</li>
		<li>eth0 MAC: {{ map_get .IndNodeMap "networking" "interfaces" "eth0" "mac" }}</li>
	<h2>Packages</h2>
		<table>
		<thead>
		<tr>
			<th>Name</th>
			<th>Version/Info</th>
		</tr>
		</thead>
		<tbody>
		{{ range $k, $v := .IndNodeMap.packages }}
		<tr>
		<td>{{ $k }}</td>
		<td>{{ $v }}</td>
		<tr>
		{{end}}
		</tbody>
		</table>

		
</body>
</html>
