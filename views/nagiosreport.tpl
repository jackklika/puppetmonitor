<!-- Nagios Human Readable -->
<!DOCTYPE html>
<html>
<head>
	<title>Puppet Monitor</title>
	{{ assets_css "/static/css/nodeview.css" }}
	{{ assets_css "/static/css/style.css" }}

	{{ assets_js "/static/js/tablesort.js" }}
	{{ assets_css "/static/css/sorttable.css" }}
</head>
<body>
	{{template "header.tpl"}}
	<div class="desc">
		<h1>Puppet Monitor - Nagios Report</h1>
		<p>This is a list of all nodes which meet the following criteria:</p>
		<ol>
			<li>Are puppetized</li>
			<li>Are part of the 61 vLAN</li>
			<li>Have a LatestReportStatus of failed OR have been inactive for {{ .nagios_maxtime }}</li>
		</ol>
	</div>

	<table class="main" id="nodes">
		<thead>
		<tr>
			<th>Name</th>
			<th>Catalog Env</th>
			<th>Last Run Status</th>
			<th>Cached Catalog Status</th>
			<th>Active?</th>
			<th>Last Cataloged</th>
			<th>Last Cataloged Δ</th>
		</thead>
		<tbody>
		{{ range .PuppetizedList }}
		<tr class="{{ .LatestReportStatus }}">
			<td><a href="/nodes/{{.Name}}">{{ .Name }}</a></td>
			<td>{{ .CatalogEnvironment }}</td>
			<td>{{ .LatestReportStatus }}</td>
			<td>{{ .CachedCatalogStatus }}</td>
			<td class="{{ .IsRecorded }}">{{ .IsRecorded }}</td>
			<td>{{ dateformat .CatalogTimestamp "Jan 2 15:04 MST" }}</td>
			<td style="background-color: rgb(255, {{ .TimeDiff }}, {{ .TimeDiff }})">{{ .CurTime }}</td>
		</tr>
		{{ end }}
		</tbody>
	</table>

	<script>
		new Tablesort(document.getElementById('nodes'));	
	</script>

</body>
</html>
