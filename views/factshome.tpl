<!DOCTYPE html>
<html>
<head>
	<title>Puppet Monitor</title>
	{{ assets_css "/static/css/style.css" }}

	{{ assets_js "/static/js/tablesort.js" }}
	{{ assets_css "/static/css/sorttable.css" }}
</head>
<body>
	{{template "header.tpl"}}
	<div class="desc">
		<p>Use this form to search for facts. Every node will be displayed with the inputted fact.</p>
		<p>Please note that currently some of these facts such as os or packages will return a map which will make the result unreadable. This will be fixed in an upcoming version when I add a method that recognizes and parses data maps.</p>
		<form action="/facts" method="post">
			Fact Search: <input type="text" name="fact">
		</form>
	</div>
	<h2>Known Facts</h2>
	<ul>
	{{ range $k, $v := .FactList }}
		<li><a href="/facts/{{$v}}">{{ $v }}</a></li>
	{{ end }}
	</ul>

</body>
</html>
