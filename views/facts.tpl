<!DOCTYPE html>
<html>
<head>
	<title>Puppet Monitor</title>

	{{ assets_css "/static/css/style.css" }}
	{{ assets_js "/static/js/tablesort.js" }}
	{{ assets_css "/static/css/sorttable.css" }}
</head>
<body>
	{{template "header.tpl"}}
	<div class="desc">
		<h1>{{ .Fact }}</h1>
		<form action="/facts" method="post">
			Fact Search: <input type="text" name="fact">
		</form>
	</div>

	<table class="main" id="facts">
		<thead>
		<tr>
			<th>Certname</th>
			<th>Fact Value</th>
		</tr>
		</thead>
		<tbody>
		{{ range .FactStruct }}
		<tr>
			<td><a href="/nodes/{{.Certname}}">{{ .Certname }}</a></td>
			<td>{{ .Value }}</td>
		</tr>
		{{ end }}
		</tbody>
	</table>

	<script>
		new Tablesort(document.getElementById('facts'));	
	</script>
</body>
</html>
