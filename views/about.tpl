<!DOCTYPE html>
<html>
<head>
	<title>About Puppetmon</title>
	{{ assets_css "/static/css/style.css" }}
</head>
<body>
	{{template "header.tpl"}}
	<div class="desc">
	<h1>About</h1>
	<p>This is a webapp written in Go and deployed by Puppet. It makes queries to puppetdb to get the needed data, and caches nodes in a json file in the working directory of this project.</p>
	<p>If any data is null, it is because puppetdb has returned it as null. If the front page is empty, this is because puppetdb is inaccessable.</p>
	<p>This project is being tracked in a <a href="https://git.ligo.org/john.klika/puppetmonitor/">git.ligo.org repo</a>. Please report any issues or request features on that page, or check out the documentation.</p>
	<p>You are using version <strong>{{.Version}}</strong> of Puppetmonitor.</p>

	<h2>check_puppetmon: Nagios Integration</h2>
	<p><a href="/nagios">/nagios</a> is a newline-seperated, cURL friendly list of all nodes that are not running puppet catalogs but should be. A script in the the git repo at /utils/<a href="https://git.ligo.org/john.klika/puppetmonitor/raw/master/utils/check_puppetmon.sh">check_puppetmon</a> is available for use as a nagios plugin which queries this endpoint. A human-friendly view of these "problem nodes" is available at <a href="/nagiosreport">/nagiosreport</a>.</p> 
	</div>
</body>
</html>
